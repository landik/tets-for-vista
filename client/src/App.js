import React, {useEffect, useState} from 'react';
import './App.css';
import './css/listsTable.css'
import './css/personTable.css'
import './fonts/stylesheet.css'
import PersonTable from "./components/PersonTable";
import ListTable from "./components/ListsTable";
import {useAjax} from "./hooks/useAjax";

function App() {
    const [present,setPresent] = useState([]);
    const [quitting,setQuitting] = useState([]);
    const [person, setPerson] = useState({});
    const { request, error, clearError } = useAjax();

    useEffect(()=>{
        if(error!== null){
        alert(error);
        clearError();
        }
    },[error,clearError]);

    // Аналог ComponentDidMount,
    useEffect(()=>{
        request('/json/presentList.json', 'GET', null,{'Content-Type':'application/json'},data => setPresent(data))
        request('/json/quittingList.json', 'GET', null, {'Content-Type':'application/json'},data => setQuitting(data))
    },[])

    let resize = false;
    let lastValue = 0,startPosition = 0,diff;
    const widthSlideBlock = 20;

    const resizeWrapper = (e) => {
        if(!resize) return;
        diff = startPosition - e.pageX + lastValue;
        document.getElementById('LeftWrapper').classList.add('wrapper__block_no-transition');
        document.getElementById('RightWrapper').classList.add('wrapper__block_no-transition');
        document.getElementById('LeftWrapper').style.width = `calc(50%  - ${diff}px - ${widthSlideBlock/2}px)`
        document.getElementById('RightWrapper').style.width = `calc(50% + ${diff}px - ${widthSlideBlock/2}px)`
    }

    const startResizeWrapper = (e) => {
        resize = true;
        startPosition = e.pageX;
    }

    const endResizeWrapper = (e) => {
        resize = false;
        lastValue = diff
        document.getElementById('LeftWrapper').classList.remove('wrapper__block_no-transition');
        document.getElementById('RightWrapper').classList.remove('wrapper__block_no-transition');
    }

    return (
      <div className="wrapper"
           onMouseMove={resizeWrapper}
           onMouseUp={endResizeWrapper}
      >
          <div id="LeftWrapper" className="wrapper__block">
              <PersonTable data={person}/>
          </div>
          <div className="wrapper__separator">
              <div className="wrapper__slider" onMouseDown={startResizeWrapper} onDragStart={()=>false}/>
          </div>
          <div id="RightWrapper" className="wrapper__block">
              <ListTable data={{present,quitting}} setPerson={setPerson}/>
          </div>
      </div>
  );
}

export default App;
