import React from 'react';
const PersonTable = (props) => {
    let name = props.data.lastName?props.data.lastName+" ":""; //Добавляем в ФИО фамилию
    name += props.data.firstName?props.data.lastName+" ":""; // Добавляем в ФИО имя
    name += props.data.patrName?props.data.patrName:""; // Добавлем в ФИО отчество

    const age = props.data.birthDate ? ((new Date().getTime() - new Date(props.data.birthDate)) / (24 * 3600 * 365.25 * 1000)) | 0 : "";

    return(<div id='personBlock' className="person-table">
            <header className="person-table__header">
                <h3 className="person-table__title">Информация о пациенте</h3>
            </header>
            <div className="person-table__content">
                <p className="person__data-item">ФИО<span>{name}</span></p>
                <p className="person__data-item">Возраст<span>{age}</span></p>
                <p className="person__data-item">Диагноз<span>{props.data.diagnosis?props.data.diagnosis:""}</span></p>
            </div>
        </div>)
}

export default PersonTable