import React, {useState} from 'react';

const ListTable = (props) => {
    const [activeTab,setActiveTab] = useState('present');

    const selectTable = tab => setActiveTab(tab);
    const selectPerson = (person) => {props.setPerson(person)};


    return (<div className="list-table">
        <header className="list-table__header">
            <ul className="tab-bar">
                <li
                    className={"tab-bar__item " + (activeTab === 'present' ? "tab-bar__item_active" : "")}
                    onClick={selectTable.bind(this,'present')}
                >
                    Присутствуют({props.data["present"].length})
                </li>
                <li
                    className={"tab-bar__item " + (activeTab === 'quitting' ? "tab-bar__item_active" : "")}
                    onClick={selectTable.bind(this,'quitting')}
                >
                    Выбывшие({props.data["quitting"].length})
                </li>
            </ul>
        </header>
        <div className="list-table__content">
            <table>
                <thead>
                    <tr>
                        <th>№ ИБ</th>
                        <th>ФИО</th>
                        <th>{activeTab === 'present'?"Палата":"Причина выбытия"}</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data[activeTab].map((item,index) => {
                        return <tr key={index} onClick={selectPerson.bind(this,item)}>
                            <td>{item.historyNumber}</td>
                            <td>{item.lastName+" "+item.firstName+" "+item.patrName}</td>
                            <td>{activeTab === 'present'?item.bedNumber:item.cause}</td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
    </div>)
}

export default ListTable