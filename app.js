const express = require('express');
const path = require('path');
const app = express();

app.use('/',express.static(path.join(__dirname,'client','build')));

app.use('/json',express.static(__dirname))

app.listen(3001, () => {
    console.log('server start on 3001 port');
})